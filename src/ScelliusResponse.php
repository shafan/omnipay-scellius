<?php

namespace Omnipay\Scellius;

class ScelliusResponse
{
    /**
     * Raw response parameters array.
     *
     * @var array[string][string]
     */
    private $rawResponse = array();

    /**
     * Certificate used to check the signature.
     *
     * @see ScelliusApi::sign
     * @var string
     */
    private $certificate;

    /**
     * Algorithm used to check the signature.
     *
     * @see ScelliusApi::sign
     * @var string
     */
    private $algo = ScelliusApi::ALGO_SHA1;

    /**
     * Value of vads_result.
     *
     * @var string
     */
    private $result;

    /**
     * Value of vads_extra_result.
     *
     * @var string
     */
    private $extraResult;

    /**
     * Value of vads_auth_result
     *
     * @var string
     */
    private $authResult;

    /**
     * Value of vads_warranty_result
     *
     * @var string
     */
    private $warrantyResult;

    /**
     * Transaction status (vads_trans_status)
     *
     * @var string
     */
    private $transStatus;

    /**
     * Constructor for ScelliusResponse class.
     * Prepare to analyse check URL or return URL call.
     *
     * @param array[string][string] $params
     * @param string $ctx_mode
     * @param string $key_test
     * @param string $key_prod
     * @param string $algo
     */
    public function __construct($params, $ctx_mode, $key_test, $key_prod, $algo = ScelliusApi::ALGO_SHA1)
    {
        $this->rawResponse = $params;
        $this->certificate = trim(($ctx_mode == 'PRODUCTION') ? $key_prod : $key_test);

        if (in_array($algo, ScelliusApi::$SUPPORTED_ALGOS)) {
            $this->algo = $algo;
        }

        // Payment results.
        $this->result = self::findInArray('vads_result', $this->rawResponse, null);
        $this->extraResult = self::findInArray('vads_extra_result', $this->rawResponse, null);
        $this->authResult = self::findInArray('vads_auth_result', $this->rawResponse, null);
        $this->warrantyResult = self::findInArray('vads_warranty_result', $this->rawResponse, null);

        $this->transStatus = self::findInArray('vads_trans_status', $this->rawResponse, null);
    }

    /**
     * Check response signature.
     * @return bool
     */
    public function isAuthentified()
    {
        return $this->getComputedSignature() == $this->getSignature();
    }

    /**
     * Return the signature computed from the received parameters, for log/debug purposes.
     * @param bool $hashed
     * @return string
     */
    public function getComputedSignature($hashed = true)
    {
        return ScelliusApi::sign($this->rawResponse, $this->certificate, $this->algo, $hashed);
    }

    /**
     * Check if the payment was successful (waiting confirmation or captured).
     * @return bool
     */
    public function isAcceptedPayment()
    {
        return in_array($this->transStatus, ScelliusApi::getSuccessStatuses()) || $this->isPendingPayment();
    }

    /**
     * Check if the payment is waiting confirmation (successful but the amount has not been
     * transfered and is not yet guaranteed).
     * @return bool
     */
    public function isPendingPayment()
    {
        return in_array($this->transStatus, ScelliusApi::getPendingStatuses());
    }

    /**
     * Return the value of a response parameter.
     * @param string $name
     * @return string
     */
    public function get($name)
    {
        // Manage shortcut notations by adding 'vads_'.
        $name = (ScelliusTools::substr($name, 0, 5) != 'vads_') ? 'vads_' . $name : $name;

        return @$this->rawResponse[$name];
    }

    /**
     * Shortcut for getting ext_info_* fields.
     * @param string $key
     * @return string
     */
    public function getExtInfo($key)
    {
        return $this->get("ext_info_$key");
    }

    /**
     * Return the expected signature received from gateway.
     * @return string
     */
    public function getSignature()
    {
        return @$this->rawResponse['signature'];
    }

    /**
     * Return a formatted string to output as a response to the notification URL call.
     *
     * @param string $case shortcut code for current situations. Most useful : payment_ok, payment_ko, auth_fail
     * @param string $extra_message some extra information to output to the payment gateway
     * @param string $original_encoding some extra information to output to the payment gateway
     * @return string
     */
    public function getOutputForGateway($case = '', $extra_message = '', $original_encoding = 'UTF-8')
    {
        // Predefined response messages according to case.
        $cases = array(
            'payment_ok' => array(true, 'Accepted payment, order has been updated.'),
            'payment_ko' => array(true, 'Payment failure, order has been cancelled.'),
            'payment_ko_bis' => array(true, 'Payment failure.'),
            'payment_ok_already_done' => array(true, 'Accepted payment, already registered.'),
            'payment_ko_already_done' => array(true, 'Payment failure, already registered.'),
            'order_not_found' => array(false, 'Order not found.'),
            'payment_ko_on_order_ok' => array(false, 'Order status does not match the payment result.'),
            'auth_fail' => array(false, 'An error occurred while computing the signature.'),
            'empty_cart' => array(false, 'Empty cart detected before order processing.'),
            'unknown_status' => array(false, 'Unknown order status.'),
            'amount_error' => array(false, 'Total paid is different from order amount.'),
            'ok' => array(true, ''),
            'ko' => array(false, '')
        );

        $success = key_exists($case, $cases) ? $cases[$case][0] : false;
        $message = key_exists($case, $cases) ? $cases[$case][1] : '';

        if (! empty($extra_message)) {
            $message .= ' ' . $extra_message;
        }

        $message = str_replace("\n", ' ', $message);

        // Set original CMS encoding to convert if necessary response to send to gateway.
        $encoding = in_array(ScelliusTools::strtoupper($original_encoding), ScelliusApi::$SUPPORTED_ENCODINGS) ?
            ScelliusTools::strtoupper($original_encoding) : 'UTF-8';
        if ($encoding !== 'UTF-8') {
            $message = iconv($encoding, 'UTF-8', $message);
        }

        $content = $success ? 'OK-' : 'KO-';
        $content .= "$message\n";

        $response = '';
        $response .= '<span style="display:none">';
        $response .= htmlspecialchars($content, ENT_COMPAT, 'UTF-8');
        $response .= '</span>';
        return $response;
    }

    public static function findInArray($key, $array, $default)
    {
        if (is_array($array) && key_exists($key, $array)) {
            return $array[$key];
        }

        return $default;
    }
}
