<?php

namespace Omnipay\Scellius;

class ScelliusTools
{
    private static $GATEWAY_URL = 'https://scelliuspaiement.labanquepostale.fr/vads-payment/';
    private static $SITE_ID = '12345678';
    private static $KEY_TEST = '1111111111111111';
    private static $KEY_PROD = '2222222222222222';
    private static $CTX_MODE = 'TEST';
    private static $SIGN_ALGO = 'SHA-256';
    private static $LANGUAGE = 'fr';

    const ON_FAILURE_RETRY = 'retry';

    const EMPTY_CART = 'empty';

    public static $submodules = array(
        'STD' => 'Standard',
    );

    public static function getDefault($name)
    {
        if (! is_string($name)) {
            return '';
        }

        if (! isset(self::$$name)) {
            return '';
        }

        return self::$$name;
    }

    /**
     * Return the list of configuration parameters with their scellius names and default values.
     *
     * @return array
     */
    public static function getAdminParameters()
    {
        // NB : keys are 32 chars max.
        $params = array(
            array('key' => 'SCELLIUS_ENABLE_LOGS', 'default' => 'True', 'label' => 'Logs'),

            array('key' => 'SCELLIUS_SITE_ID', 'name' => 'site_id', 'default' => self::getDefault('SITE_ID'), 'label' => 'Site ID'),
            array('key' => 'SCELLIUS_KEY_TEST', 'name' => 'key_test', 'default' => self::getDefault('KEY_TEST'),
            'label' => 'Certificate in test mode'),
            array('key' => 'SCELLIUS_KEY_PROD', 'name' => 'key_prod', 'default' => self::getDefault('KEY_PROD'),
            'label' => 'Certificate in production mode'),
            array('key' => 'SCELLIUS_MODE', 'name' => 'ctx_mode', 'default' => self::getDefault('CTX_MODE'), 'label' => 'Mode'),
            array('key' => 'SCELLIUS_SIGN_ALGO', 'name' => 'sign_algo', 'default' => self::getDefault('SIGN_ALGO'),
            'label' => 'Signature algorithm'),
            array('key' => 'SCELLIUS_PLATFORM_URL', 'name' => 'platform_url',
            'default' => self::getDefault('GATEWAY_URL'), 'label' => 'Payment page URL'),

            array('key' => 'SCELLIUS_PRIVKEY_TEST', 'default' => '', 'label' => 'Test password'),
            array('key' => 'SCELLIUS_PRIVKEY_PROD', 'default' => '', 'label' => 'Production password'),
            array('key' => 'SCELLIUS_PUBKEY_TEST', 'default' => '', 'label' => 'Public test key'),
            array('key' => 'SCELLIUS_PUBKEY_PROD', 'default' => '', 'label' => 'Public production key'),
            array('key' => 'SCELLIUS_RETKEY_TEST', 'default' => '', 'label' => 'SHA256 test key'),
            array('key' => 'SCELLIUS_RETKEY_PROD', 'default' => '', 'label' => 'SHA256 production key'),

            array('key' => 'SCELLIUS_DEFAULT_LANGUAGE', 'default' => self::getDefault('LANGUAGE'), 'label' => 'Default language'),
            array('key' => 'SCELLIUS_AVAILABLE_LANGUAGES', 'name' => 'available_languages', 'default' => '',
            'label' => 'Available languages'),
            array('key' => 'SCELLIUS_DELAY', 'name' => 'capture_delay', 'default' => '', 'label' => 'Capture delay'),
            array('key' => 'SCELLIUS_VALIDATION_MODE', 'name' => 'validation_mode', 'default' => '',
            'label' => 'Payment validation'),

            array('key' => 'SCELLIUS_SHOP_NAME', 'name' => 'shop_name', 'default' => '', 'label' => 'Shop name'),
            array('key' => 'SCELLIUS_SHOP_URL', 'name' => 'shop_url', 'default' => '', 'label' => 'Shop URL'),

            array('key' => 'SCELLIUS_3DS_MIN_AMOUNT', 'default' => '', 'label' => 'Disable 3DS by customer group'),

            array('key' => 'SCELLIUS_REDIRECT_ENABLED', 'name' => 'redirect_enabled', 'default' => 'False',
            'label' => 'Automatic redirection'),
            array('key' => 'SCELLIUS_REDIRECT_SUCCESS_T', 'name' => 'redirect_success_timeout', 'default' => '5',
            'label' => 'Redirection timeout on success'),
            array('key' => 'SCELLIUS_REDIRECT_SUCCESS_M', 'name' => 'redirect_success_message',
            'default' => 'Redirection vers la boutique dans quelques instants...',
            'label' => 'Redirection message on success'),
            array('key' => 'SCELLIUS_REDIRECT_ERROR_T', 'name' => 'redirect_error_timeout', 'default' => '5',
            'label' => 'Redirection timeout on failure'),
            array('key' => 'SCELLIUS_REDIRECT_ERROR_M', 'name' => 'redirect_error_message',
            'default' => 'Redirection vers la boutique dans quelques instants...',
            'label' => 'Redirection message on failure'),
            array('key' => 'SCELLIUS_RETURN_MODE', 'name' => 'return_mode', 'default' => 'GET',
            'label' => 'Return mode'),
            array('key' => 'SCELLIUS_FAILURE_MANAGEMENT', 'default' => self::ON_FAILURE_RETRY,
            'label' => 'Payment failed management'),
            array('key' => 'SCELLIUS_CART_MANAGEMENT', 'default' => self::EMPTY_CART, 'label' => 'Cart management'),

            array('key' => 'SCELLIUS_SEND_CART_DETAIL', 'default' => 'True', 'label' => 'Send shopping cart details'),
            array('key' => 'SCELLIUS_COMMON_CATEGORY', 'default' => 'FOOD_AND_GROCERY',
            'label' => 'Category mapping'),
            array('key' => 'SCELLIUS_CATEGORY_MAPPING', 'default' => array(), 'label' => 'Category mapping'),
            array('key' => 'SCELLIUS_SEND_SHIP_DATA', 'default' => 'False',
            'label' => 'Always send advanced shipping data'),
            array('key' => 'SCELLIUS_ONEY_SHIP_OPTIONS', 'default' => array(), 'label' => 'Shipping options'),

            array('key' => 'SCELLIUS_STD_TITLE',
            'default' => 'Paiement par carte bancaire',
            'label' => 'Method title'),
            array('key' => 'SCELLIUS_STD_ENABLED', 'default' => 'True', 'label' => 'Activation'),
            array('key' => 'SCELLIUS_STD_DELAY', 'default' => '', 'label' => 'Capture delay'),
            array('key' => 'SCELLIUS_STD_VALIDATION', 'default' => '-1', 'label' => 'Payment validation'),
            array('key' => 'SCELLIUS_STD_PAYMENT_CARDS', 'default' => '', 'label' => 'Card Types'),
            array('key' => 'SCELLIUS_STD_PROPOSE_ONEY', 'default' => 'False', 'label' => 'Propose FacilyPay Oney'),
            array('key' => 'SCELLIUS_STD_AMOUNTS', 'default' => array(), 'label' => 'Standard payment - Customer group amount restriction'),
            array('key' => 'SCELLIUS_STD_CARD_DATA_MODE', 'default' => '1', 'label' => 'Card data entry mode'),
            array('key' => 'SCELLIUS_STD_REST_DISPLAY_MODE', 'default' => 'embedded', 'label' => 'Display mode'),
            array('key' => 'SCELLIUS_STD_REST_THEME', 'default' => 'material', 'label' => 'Custom theme'),
            array('key' => 'SCELLIUS_STD_REST_PLACEHLDR', 'default' => array(), 'label' => 'Custom field placeholders'),
            array('key' => 'SCELLIUS_STD_REST_ATTEMPTS', 'default' => '', 'label' => 'Payment attempts number'),
            array('key' => 'SCELLIUS_STD_1_CLICK_PAYMENT', 'default' => 'False', 'label' => 'Payment by token'),
            array('key' => 'SCELLIUS_STD_CANCEL_IFRAME', 'default' => 'False', 'label' => 'Cancel payment in iframe mode'),

            array('key' => 'SCELLIUS_MULTI_TITLE',
            'default' => 'Paiement par carte bancaire en plusieurs fois',
            'label' => 'Method title'),
            array('key' => 'SCELLIUS_MULTI_ENABLED', 'default' => 'False', 'label' => 'Activation'),
            array('key' => 'SCELLIUS_MULTI_DELAY', 'default' => '', 'label' => 'Capture delay'),
            array('key' => 'SCELLIUS_MULTI_VALIDATION', 'default' => '-1', 'label' => 'Payment validation'),
            array('key' => 'SCELLIUS_MULTI_PAYMENT_CARDS', 'default' => '', 'label' => 'Card Types'),
            array('key' => 'SCELLIUS_MULTI_CARD_MODE', 'default' => '1', 'label' => 'Card selection mode'),
            array('key' => 'SCELLIUS_MULTI_AMOUNTS', 'default' => array(), 'label' => 'Payment in installments - Customer group amount restriction'),
            array('key' => 'SCELLIUS_MULTI_OPTIONS', 'default' => array(), 'label' => 'Payment in installments - Payment options'),

            array('key' => 'SCELLIUS_ONEY_TITLE',
            'default' => 'Paiement avec FacilyPay Oney (Déprécié)',
            'label' => 'Method title'),
            array('key' => 'SCELLIUS_ONEY_ENABLED', 'default' => 'False', 'label' => 'Activation'),
            array('key' => 'SCELLIUS_ONEY_DELAY', 'default' => '', 'label' => 'Capture delay'),
            array('key' => 'SCELLIUS_ONEY_VALIDATION', 'default' => '-1', 'label' => 'Payment validation'),
            array('key' => 'SCELLIUS_ONEY_AMOUNTS', 'default' => array(), 'label' => 'FacilyPay Oney payment - Customer group amount restriction'),
            array('key' => 'SCELLIUS_ONEY_ENABLE_OPTIONS', 'default' => 'False',
            'label' => 'Enable options selection'),
            array('key' => 'SCELLIUS_ONEY_OPTIONS', 'default' => array(), 'label' => 'FacilyPay Oney payment - Payment options'),

            array('key' => 'SCELLIUS_ONEY34_TITLE',
            'default' => 'Paiement en 3 ou 4 fois Oney',
            'label' => 'Method title'),
            array('key' => 'SCELLIUS_ONEY34_ENABLED', 'default' => 'False', 'label' => 'Activation'),
            array('key' => 'SCELLIUS_ONEY34_DELAY', 'default' => '', 'label' => 'Capture delay'),
            array('key' => 'SCELLIUS_ONEY34_VALIDATION', 'default' => '-1', 'label' => 'Payment validation'),
            array('key' => 'SCELLIUS_ONEY34_AMOUNTS', 'default' => array(), 'label' => 'Payment in 3 or 4 times Oney - Customer group amount restriction'),
            array('key' => 'SCELLIUS_ONEY34_ENABLE_OPTIONS', 'default' => 'False',
            'label' => 'Enable options selection'),
            array('key' => 'SCELLIUS_ONEY34_OPTIONS', 'default' => array(), 'label' => 'Payment in 3 or 4 times Oney - Payment options'),

            array('key' => 'SCELLIUS_FULLCB_TITLE',
            'default' => 'Paiement avec Full CB',
            'label' => 'Method title'),
            array('key' => 'SCELLIUS_FULLCB_ENABLED', 'default' => 'False', 'label' => 'Activation'),
            array('key' => 'SCELLIUS_FULLCB_DELAY', 'default' => '', 'label' => 'Capture delay'),
            array('key' => 'SCELLIUS_FULLCB_VALIDATION', 'default' => '-1', 'label' => 'Payment validation'),
            array('key' => 'SCELLIUS_FULLCB_AMOUNTS',
            'default' => array(
                array('min_amount' => '100', 'max_amount' => '1500')
            ),
            'label' => 'Full CB payment - Customer group amount restriction'),
            array('key' => 'SCELLIUS_FULLCB_ENABLE_OPTS', 'default' => 'False',
            'label' => 'Enable options selection'),
            array('key' => 'SCELLIUS_FULLCB_OPTIONS',
            'default' => array(
                'FULLCB3X' => array(
                    'enabled' => 'True',
                    'label' => 'Paiement en 3 fois',
                    'min_amount' => '',
                    'max_amount' => '',
                    'rate' => '1.4',
                    'cap' => '9',
                    'count' => '3'
                ),
                'FULLCB4X' => array(
                    'enabled' => 'True',
                    'label' => 'Paiement en 4 fois',
                    'min_amount' => '',
                    'max_amount' => '',
                    'rate' => '2.1',
                    'cap' => '12',
                    'count' => '4'
                )
            ),
            'label' => 'Full CB payment - Payment options'),

            array('key' => 'SCELLIUS_ANCV_TITLE',
            'default' => 'Paiement avec ANCV',
            'label' => 'Method title'),
            array('key' => 'SCELLIUS_ANCV_ENABLED', 'default' => 'False', 'label' => 'Activation'),
            array('key' => 'SCELLIUS_ANCV_DELAY', 'default' => '', 'label' => 'Capture delay'),
            array('key' => 'SCELLIUS_ANCV_VALIDATION', 'default' => '-1', 'label' => 'Payment validation'),
            array('key' => 'SCELLIUS_ANCV_AMOUNTS', 'default' => array(), 'label' => 'ANCV payment - Customer group amount restriction'),

            array('key' => 'SCELLIUS_SEPA_TITLE',
            'default' => 'Paiement avec SEPA',
            'label' => 'Method title'),
            array('key' => 'SCELLIUS_SEPA_ENABLED', 'default' => 'False', 'label' => 'Activation'),
            array('key' => 'SCELLIUS_SEPA_DELAY', 'default' => '', 'label' => 'Capture delay'),
            array('key' => 'SCELLIUS_SEPA_VALIDATION', 'default' => '-1', 'label' => 'Payment validation'),
            array('key' => 'SCELLIUS_SEPA_AMOUNTS', 'default' => array(), 'label' => 'SEPA payment - Customer group amount restriction'),
            array('key' => 'SCELLIUS_SEPA_MANDATE_MODE', 'default' => 'PAYMENT', 'label' => 'SEPA direct debit mode'),

            array('key' => 'SCELLIUS_SOFORT_TITLE',
            'default' => 'Paiement avec SOFORT Banking',
            'label' => 'Method title'),
            array('key' => 'SCELLIUS_SOFORT_ENABLED', 'default' => 'False', 'label' => 'Activation'),
            array('key' => 'SCELLIUS_SOFORT_AMOUNTS', 'default' => array(), 'label' => 'SOFORT Banking payment - Customer group amount restriction'),

            array('key' => 'SCELLIUS_PAYPAL_TITLE',
            'default' => 'Paiement avec PayPal',
            'label' => 'Method title'),
            array('key' => 'SCELLIUS_PAYPAL_ENABLED', 'default' => 'False', 'label' => 'Activation'),
            array('key' => 'SCELLIUS_PAYPAL_DELAY', 'default' => '', 'label' => 'Capture delay'),
            array('key' => 'SCELLIUS_PAYPAL_VALIDATION', 'default' => '-1', 'label' => 'Payment validation'),
            array('key' => 'SCELLIUS_PAYPAL_AMOUNTS', 'default' => array(), 'label' => 'PayPal payment - Customer group amount restriction'),

            array('key' => 'SCELLIUS_CHOOZEO_TITLE',
            'default' => 'Paiement avec Choozeo sans frais',
            'label' => 'Method title'),
            array('key' => 'SCELLIUS_CHOOZEO_ENABLED', 'default' => 'False', 'label' => 'Activation'),
            array('key' => 'SCELLIUS_CHOOZEO_DELAY', 'default' => '', 'label' => 'Capture delay'),
            array('key' => 'SCELLIUS_CHOOZEO_AMOUNTS',
            'default' => array(
                array('min_amount' => '135', 'max_amount' => '2000')
            ),
            'label' => 'Choozeo payment - Customer group amount restriction'),
            array('key' => 'SCELLIUS_CHOOZEO_OPTIONS', 'default' => array(
                'EPNF_3X' => array(
                    'enabled' => 'True',
                    'min_amount' => '',
                    'max_amount' => ''
                ),
                'EPNF_4X' => array(
                    'enabled' => 'True',
                    'min_amount' => '',
                    'max_amount' => ''
                )
            ), 'label' => 'Choozeo payment - Payment options'),

            array('key' => 'SCELLIUS_OTHER_GROUPED_VIEW', 'default' => 'False', 'label' => 'Regroup payment means'),
            array('key' => 'SCELLIUS_OTHER_ENABLED', 'default' => 'True', 'label' => 'Activation'),
            array('key' => 'SCELLIUS_OTHER_TITLE',
            'default' => 'Autres moyens de paiement',
            'label' => 'Method title'),
            array('key' => 'SCELLIUS_OTHER_AMOUNTS', 'default' => array(), 'label' => 'Other payment means - Customer group amount restriction'),
            array('key' => 'SCELLIUS_OTHER_PAYMENT_MEANS', 'default' => array(), 'label' => 'Other payment means - Payment means')
        );

        foreach (array_keys(self::$submodules) as $key) {
            array_push($params, array('key' => 'SCELLIUS_' . $key . '_COUNTRY', 'default' => '1', 'label' => 'Restrict to some countries'));
            array_push($params, array('key' => 'SCELLIUS_' . $key . '_COUNTRY_LST', 'default' => '', 'label' => 'Authorized countries'));
        }

        return $params;
    }

    public static function convertRestResult($answer, $isTransaction = false)
    {
        if (! is_array($answer) || empty($answer)) {
            return array();
        }

        if ($isTransaction) {
            $transaction = $answer;
        } else {
            $transactions = self::getProperty($answer, 'transactions');

            if (! is_array($transactions) || empty($transactions)) {
                return array();
            }

            $transaction = $transactions[0];
        }

        $response = array();

        $response['vads_result'] = self::getProperty($transaction, 'errorCode') ? self::getProperty($transaction, 'errorCode') : '00';
        $response['vads_extra_result'] = self::getProperty($transaction, 'detailedErrorCode');

        $response['vads_trans_status'] = self::getProperty($transaction, 'detailedStatus');
        $response['vads_trans_uuid'] = self::getProperty($transaction, 'uuid');
        $response['vads_operation_type'] = self::getProperty($transaction, 'operationType');
        $response['vads_effective_creation_date'] = self::getProperty($transaction, 'creationDate');
        $response['vads_payment_config'] = 'SINGLE'; // Only single payments are possible via REST API at this time.

        if (($customer = self::getProperty($answer, 'customer')) && ($billingDetails = self::getProperty($customer, 'billingDetails'))) {
            $response['vads_language'] = self::getProperty($billingDetails, 'language');
        }

        $response['vads_amount'] = self::getProperty($transaction, 'amount');
        $response['vads_currency'] = ScelliusApi::getCurrencyNumCode(self::getProperty($transaction, 'currency'));

        if (self::getProperty($transaction, 'paymentMethodToken')) {
            $response['vads_identifier'] = self::getProperty($transaction, 'paymentMethodToken');
            $response['vads_identifier_status'] = 'CREATED';
        }

        if ($orderDetails = self::getProperty($answer, 'orderDetails')) {
            $response['vads_order_id'] = self::getProperty($orderDetails, 'orderId');
        }

        if ($metadata = self::getProperty($transaction, 'metadata')) {
            $orderInfo = key_exists('orderInfo', $metadata) ? self::getProperty($metadata, 'orderInfo') :
                self::getProperty($metadata, 'info');
            $response['vads_order_info'] = $orderInfo;
        }

        if ($transactionDetails = self::getProperty($transaction, 'transactionDetails')) {
            $response['vads_sequence_number'] = self::getProperty($transactionDetails, 'sequenceNumber');

            // Workarround to adapt to REST API behavior.
            $effectiveAmount = self::getProperty($transactionDetails, 'effectiveAmount');
            $effectiveCurrency = ScelliusApi::getCurrencyNumCode(self::getProperty($transactionDetails, 'effectiveCurrency'));

            if ($effectiveAmount && $effectiveCurrency) {
                $response['vads_effective_amount'] = $response['vads_amount'];
                $response['vads_effective_currency'] = $response['vads_currency'];
                $response['vads_amount'] = $effectiveAmount;
                $response['vads_currency'] = $effectiveCurrency;
            }

            $response['vads_warranty_result'] = self::getProperty($transactionDetails, 'liabilityShift');

            if ($cardDetails = self::getProperty($transactionDetails, 'cardDetails')) {
                $response['vads_trans_id'] = self::getProperty($cardDetails, 'legacyTransId'); // Deprecated.
                $response['vads_presentation_date'] = self::getProperty($cardDetails, 'expectedCaptureDate');

                $response['vads_card_brand'] = self::getProperty($cardDetails, 'effectiveBrand');
                $response['vads_card_number'] = self::getProperty($cardDetails, 'pan');
                $response['vads_expiry_month'] = self::getProperty($cardDetails, 'expiryMonth');
                $response['vads_expiry_year'] = self::getProperty($cardDetails, 'expiryYear');

                if ($authorizationResponse = self::getProperty($cardDetails, 'authorizationResponse')) {
                    $response['vads_auth_result'] = self::getProperty($authorizationResponse, 'authorizationResult');
                }

                if (($authenticationResponse = self::getProperty($cardDetails, 'authenticationResponse'))
                    && ($value = self::getProperty($authenticationResponse, 'value'))) {
                    $response['vads_threeds_status'] = self::getProperty($value, 'status');
                    $response['vads_threeds_auth_type'] = self::getProperty($value, 'authenticationType');
                    if ($authenticationValue = self::getProperty($value, 'authenticationValue')) {
                        $response['vads_threeds_cavv'] = self::getProperty($authenticationValue, 'value');
                    }
                } elseif (($threeDSResponse = self::getProperty($cardDetails, 'threeDSResponse'))
                    && ($authenticationResultData = self::getProperty($threeDSResponse, 'authenticationResultData'))) {
                    $response['vads_threeds_cavv'] = self::getProperty($authenticationResultData, 'cavv');
                    $response['vads_threeds_status'] = self::getProperty($authenticationResultData, 'status');
                    $response['vads_threeds_auth_type'] = self::getProperty($authenticationResultData, 'threeds_auth_type');
                }
            }

            if ($fraudManagement = self::getProperty($transactionDetails, 'fraudManagement')) {
                if ($riskControl = self::getProperty($fraudManagement, 'riskControl')) {
                    $response['vads_risk_control'] = '';

                    foreach ($riskControl as $value) {
                        $response['vads_risk_control'] .= "{$value['name']}={$value['result']};";
                    }
                }

                if ($riskAssessments = self::getProperty($fraudManagement, 'riskAssessments')) {
                    $response['vads_risk_assessment_result'] = self::getProperty($riskAssessments, 'results');
                }
            }
        }

        return $response;
    }

    private static function getProperty($array, $key)
    {
        if (isset($array[$key])) {
            return $array[$key];
        }

        return null;
    }

    public static function checkFormIpnValidity()
    {
        $jinput = JFactory::getApplication()->input;
        return $jinput->get('vads_hash');
    }

    public static function substr($str, $start, $length = false, $encoding = 'utf-8')
    {
        if (is_array($str)) {
            return false;
        }
        if (function_exists('mb_substr')) {
            return mb_substr($str, (int) $start, ($length === false ? self::strlen($str) : (int) $length), $encoding);
        }

        return substr($str, $start, ($length === false ? self::strlen($str) : (int) $length));
    }

    public static function strtolower($str)
    {
        if (is_array($str)) {
            return false;
        }
        if (function_exists('mb_strtolower')) {
            return mb_strtolower($str, 'utf-8');
        }

        return strtolower($str);
    }

    static function strtoupper($str)
    {
        if (is_array($str))
            return false;
        if (function_exists('mb_strtoupper'))
            return mb_strtoupper($str, 'utf-8');
        return strtoupper($str);
    }

    public static function strlen($str, $encoding = 'UTF-8')
    {
        if (is_array($str)) {
            return false;
        }
        $str = html_entity_decode($str, ENT_COMPAT, 'UTF-8');
        if (function_exists('mb_strlen')) {
            return mb_strlen($str, $encoding);
        }

        return strlen($str);
    }
}
