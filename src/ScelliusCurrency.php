<?php

namespace Omnipay\Scellius;

class ScelliusCurrency
{

    private $alpha3;
    private $num;
    private $decimals;

    public function __construct($alpha3, $num, $decimals = 2)
    {
        $this->alpha3 = $alpha3;
        $this->num = $num;
        $this->decimals = $decimals;
    }

    public function convertAmountToInteger($float)
    {
        $coef = pow(10, $this->decimals);

        $amount = $float * $coef;
        return (int) (string) $amount; // Cast amount to string (to avoid rounding) than return it as int.
    }

    public function convertAmountToFloat($integer)
    {
        $coef = pow(10, $this->decimals);

        return ((float) $integer) / $coef;
    }

    public function getAlpha3()
    {
        return $this->alpha3;
    }

    public function getNum()
    {
        return $this->num;
    }

    public function getDecimals()
    {
        return $this->decimals;
    }
}
