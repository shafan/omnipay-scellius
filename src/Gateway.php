<?php

namespace Omnipay\Scellius;

use Omnipay\Common\AbstractGateway;

/**
 * Scellius Gateway
 */
class Gateway extends AbstractGateway
{
    public function getName()
    {
        return 'Scellius';
    }

    public function getDefaultParameters()
    {
        return array(
            'merchantId'=> '',
            'merchantCountry'=> '',
            'keyTest'=> '',
            'keyProd'=> '',
            'mode'=> '',
        );
    }

    public function getMerchantId()
    {
        return $this->getParameter('merchantId');
    }

    public function setMerchantId($value)
    {
        return $this->setParameter('merchantId', $value);
    }

    public function getMerchantCountry()
    {
        return $this->getParameter('merchantCountry');
    }

    public function setMerchantCountry($value)
    {
        return $this->setParameter('merchantCountry', $value);
    }

    public function getKeyTest()
    {
        return $this->getParameter('keyTest');
    }

    public function setKeyTest($value)
    {
        return $this->setParameter('keyTest', $value);
    }

    public function getKeyProd()
    {
        return $this->getParameter('keyProd');
    }

    public function setKeyProd($value)
    {
        return $this->setParameter('keyProd', $value);
    }

    public function getMode()
    {
        return $this->getParameter('mode');
    }

    public function setMode($value)
    {
        return $this->setParameter('mode', $value);
    }

    public function getCustomerEmail()
    {
        return $this->getParameter('customerEmail');
    }

    public function setCustomerEmail($value)
    {
        return $this->setParameter('customerEmail', $value);
    }

    public function getCustomerName()
    {
        return $this->getParameter('customerName');
    }

    public function setCustomerName($value)
    {
        return $this->setParameter('customerName', $value);
    }
    
    public function getCustomerFirstname()
    {
        return $this->getParameter('customerFirstname');
    }
    public function setCustomerFirstname($value)
    {
        return $this->setParameter('customerFirstname', $value);
    }

    public function getCustomerPhone()
    {
        return $this->getParameter('customerPhone');
    }
    public function setCustomerPhone($value)
    {
        return $this->setParameter('customerPhone', $value);
    }

    public function purchase(array $parameters = array())
    {
        return $this->createRequest('\Omnipay\Scellius\Message\PurchaseRequest', $parameters);
    }

    public function completePurchase(array $parameters = array())
    {
        return $this->createRequest('\Omnipay\Scellius\Message\CompletePurchaseRequest', $parameters);
    }
}
