<?php

namespace Omnipay\Scellius\Message;

use Omnipay\Common\Message\AbstractRequest;
use Omnipay\Scellius\ScelliusRequest;
use Omnipay\Scellius\ScelliusTools;
use Omnipay\Scellius\Helper;

/**
 * Scellius Purchase Request
 */
class PurchaseRequest extends AbstractRequest
{
    public const SCELLIUS_REDIRECT_ENABLED = true;
    public const SCELLIUS_REDIRECT_SUCCESS_T = 1;
    public const SCELLIUS_REDIRECT_ERROR_T = 1;
    public const SCELLIUS_RETURN_MODE = "POST";
    public const  DEVISE = 'EUR'; // ne semble pas utilisé
                                  //
    public function getMerchantId()
    {
        return $this->getParameter('merchantId');
    }

    public function setMerchantId($value)
    {
        return $this->setParameter('merchantId', $value);
    }

    public function getMerchantCountry()
    {
        return $this->getParameter('merchantCountry');
    }

    public function setMerchantCountry($value)
    {
        return $this->setParameter('merchantCountry', $value);
    }

    public function getKeyTest()
    {
        return $this->getParameter('keyTest');
    }

    public function setKeyTest($value)
    {
        return $this->setParameter('keyTest', $value);
    }

    public function getKeyProd()
    {
        return $this->getParameter('keyProd');
    }

    public function setKeyProd($value)
    {
        return $this->setParameter('keyProd', $value);
    }

    public function getMode()
    {
        return $this->getParameter('mode');
    }

    public function setMode($value)
    {
        return $this->setParameter('mode', $value);
    }

    public function getSession()
    {
        return $this->getParameter('session');
    }

    public function setSession($value)
    {
        return $this->setParameter('session', $value);
    }

    public function getCustomerEmail()
    {
        return $this->getParameter('customerEmail');
    }

    public function setCustomerEmail($value)
    {
        return $this->setParameter('customerEmail', $value);
    }

    public function getCustomerName()
    {
        return $this->getParameter('customerName');
    }

    public function setCustomerName($value)
    {
        return $this->setParameter('customerName', $value);
    }
    
    public function getCustomerFirstname()
    {
        return $this->getParameter('customerFirstname');
    }
    public function setCustomerFirstname($value)
    {
        return $this->setParameter('customerFirstname', $value);
    }

    public function getCustomerPhone()
    {
        return $this->getParameter('customerPhone');
    }
    public function setCustomerPhone($value)
    {
        return $this->setParameter('customerPhone', $value);
    }

    /**
     * Get the request automatic return URL.
     *
     * @return string
     */
    public function getAutomaticReturnUrl()
    {
        return $this->getParameter('automaticReturnUrl');
    }

    /**
     * Sets the request automatic return URL.
     *
     */
    public function setAutomaticReturnUrl($value)
    {
        return $this->setParameter('automaticReturnUrl', $value);
    }

    /**
     * Get the cancel return URL.
     *
     * @return string
     */
    public function getCancelUrl()
    {
        return $this->getParameter('cancelUrl');
    }

    /**
     * Sets the request cancel return URL.
     *
     */
    public function setCancelUrl($value)
    {
        return $this->setParameter('cancelUrl', $value);
    }

    /**
     * Get the return URL.
     *
     * @return string
     */
    public function getReturnUrl()
    {
        return $this->getParameter('returnUrl');
    }

    //obligatoire
    public function getData()
    {
        $this->validate('amount');

        if (!$this->getNotifyUrl()) {
            $this->validate('returnUrl');
        }

        $request = new ScelliusRequest();
        foreach (ScelliusTools::getAdminParameters() as $param) {
            if (array_key_exists('name', $param)) {
                if (array_key_exists('default', $param)) {
                    $request->set($param['name'], $param['default']);
                }
                if (defined('self::'.$param['key'])) {
                    $value = constant('self::'.$param['key']);
                    $request->set($param['name'], $value);
                }
            }
        }
        $request->set('cust_email', $this->getCustomerEmail());
        $request->set('cust_first_name', $this->getCustomerFirstname());
        $request->set('cust_last_name', $this->getCustomerName());
        $request->set('cust_phone', $this->getCustomerPhone());
        $request->set('site_id', $this->getMerchantId());
        $request->set('key_test', $this->getKeyTest());
        $request->set('key_prod', $this->getKeyProd());
        $request->set('ctx_mode', $this->getMode());
        $request->set('amount', $this->getAmountInteger());
        $request->set('currency', $this->getCurrencyNumeric());
        $request->set('language', $this->getMerchantCountry());
        $request->set('url_return', $this->getReturnUrl());
        $request->set('url_cancel', $this->getCancelUrl());
        $request->set('url_error', $this->getReturnUrl());
        $request->set('url_refused', $this->getReturnUrl());
        $request->set('url_success', $this->getAutomaticReturnUrl());
        $request->set('order_id', $this->getTransactionReference());
        $request->set('trans_id', $this->getTransactionId());
        $request->addExtInfo('session', $this->getSession());

        return $request;
    }

    //obligatoire
    public function sendData($data)
    {
        return $this->response = new PurchaseResponse($this, $data);
    }
}
