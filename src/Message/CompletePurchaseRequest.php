<?php 

namespace Omnipay\Scellius\Message;

use Omnipay\Common\Exception\InvalidResponseException;
use Omnipay\Scellius\ScelliusResponse;
use Omnipay\Scellius\ScelliusTools;

/**
 * Scellius Complete Purchase Request
 */
class CompletePurchaseRequest extends PurchaseRequest
{
    public function getData()
    {
        $response = new ScelliusResponse(
            $_REQUEST,
            $this->getMode(),
            $this->getKeyTest(),
            $this->getKeyProd(),
            ScelliusTools::getDefault('SIGN_ALGO')
        );

        return $response;
    }

    public function sendData($data)
    {
        return $this->response = new CompletePurchaseResponse($this, $data);
    }
}
