<?php

namespace Omnipay\Scellius\Message;

use Omnipay\Common\Message\AbstractResponse;
use Omnipay\Common\Message\RedirectResponseInterface;
use Omnipay\Common\Exception\RuntimeException;
use Symfony\Component\HttpFoundation\Response as HttpResponse;

/**
 * Scellius Purchase Response
 */
class PurchaseResponse extends AbstractResponse implements RedirectResponseInterface
{
    public function isSuccessful()
    {
        return false;
    }

    public function isRedirect()
    {
        return true;
    }

    //obligatoire
    public function getRedirectUrl()
    {
        return $this->request->getReturnUrl();
    }

    //obligatoire
    public function getRedirectMethod()
    {
        return 'POST';
    }

    //obligatoire
    public function getRedirectData()
    {
        return parent::getRedirectData();
    }

    public function getRedirectContentScellius()
    {
        $scellius_form = $this->getData()->getRequestHtmlForm(
            '',
            'hidden',
            '',
            'submit',
            'Payer'
        );

        return ['success' => true, 'message' => $scellius_form];
    }

    public function getRedirectResponse()
    {
        if (!$this instanceof RedirectResponseInterface || !$this->isRedirect()) {
            throw new RuntimeException('This response does not support redirection.');
        }

        $content = $this->getRedirectContentScellius();

        $output = '<!DOCTYPE html>
<html>
    <head>
        <title>Redirecting...</title>
    </head>
    <body">
        %1$s
    </body>
</html>';
        $output = sprintf(
            $output,
            $content['message']
        );

        return HttpResponse::create($output);
    }
}

