<?php 

namespace Omnipay\Scellius\Message;

use Omnipay\Common\Message\AbstractResponse;

/**
 * Scellius Complete Purchase Response
 */
class CompletePurchaseResponse extends AbstractResponse
{
    public function isSuccessful()
    {
        return $this->data->isAuthentified() && $this->data->isAcceptedPayment();
    }

    public function isCancelled()
    {
        return $this->data->get('trans_status') === 'ABANDONED';
    }

    public function getTransactionId()
    {
        return $this->data->get('trans_id');
    }

    public function getOrderId()
    {
        return $this->data->get('order_id');
    }

    public function getMessage()
    {
        return $this->data->getOutputForGateway();
    }
    
    /**
     * Optional step: Redirect the customer back to your own domain.
     *
     * This is achieved by returning a HTML string containing a meta-redirect which is displayed by WorldPay
     * to the customer. This is far from ideal, but apparently (according to their support) this is the only
     * method currently available.
     *
     * @param string $returnUrl The URL to forward the customer to.
     * @param string|null $message   Optional message to display to the customer before they are redirected.
     */
    public function confirm($returnUrl, $message = null)
    {
        if (empty($message)) {
            $message = 'Thank you, your transaction has been processed. You are being redirected...';
        }
        echo '<meta http-equiv="refresh" content="2;url='.$returnUrl.'" /><p>'.$message.'</p>';
        exit;
    }
}
